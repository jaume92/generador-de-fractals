# Generador de Fractals mitjançant el mètode de Newton #

És tracta d'un problema que vaig entregar per l'assignatura de mètodes numèrics l'any 2013.

Aquest programa permet generar fractals utilitzant el mètode de Newton. Per a un polinomi a coeficients complexos, s'estudiarà la convergència de diverses llavors sobre el pla complex. El valor de l'arrel a la que convergeixi cada llavor serà representada amb un color diferent, fet que generarà el fractal. 

### Executar el codi ###

**1.** Al terminal introduir ``$ make`` per compilar els scripts.

**2.** Executar el programa: ``$ ./dibfr n x0 xf nx y0 yf ny tolcnv maxit > fractal.txt ``. On:

* *n*: Nombre d'arrels complexes del polinomi
* *x0, xf, y0, yf*: Defineixen el rectangle sobre el pla complex que utilitza els seus valors com a llavors.
* *nx, ny*: Nombre de punts que prenem com llavors sobre l'eix *x* i *y*.
* *tolcnv, maxit*: Defineixen el límit de convergència i el màxim nombre d'iteracions.

**3.** Un cop executant-se el programa demanarà una llista amb: component real de l'arrel, component imaginària de l'arrel, tres valors RGB normalitzats a 1 (per introduir el color).

**4.** Obrir el *gnuplot*

**5.** Dins del *gnuplot* introduir 
```
#!
> unset key
> plot ’fractal.txt’ w rgbimage
```

### Exemple ###
Pel polinomi: *p(z) = z^3-1*

```
#!
./dibfr 3=n -2=x0 2=xf 640=nx -2=y0 2=yf 480=ny 1e-10=tolcnv 50=maxit > fractal.txt

1 0 1 0 0
-0.5 .8660254037844386 0 1 0
-0.5 -.8660254037844386 0 0 1
```

![fractal1.png](https://bitbucket.org/repo/bEGKjq/images/814879884-fractal1.png)


### Altres Resultats ###
Polinomi: *p(z) = z^8 + 15z^4 - 16*
![fractal2.png](https://bitbucket.org/repo/bEGKjq/images/4237494489-fractal2.png)