#include <stdio.h>
#include <stdlib.h>
#include "nwtfr.h"

#define MAX 250

int main (int argc, char *argv[])
{
   int i,j, n,maxit,k;
   double r[MAX], g[MAX], b[MAX], Dx, Dy, x, y,u[MAX],v[MAX], x0, xf, nx, y0, yf, ny, tolcnv;
  
   if (argc!=10
      || sscanf(argv[1], "%d", &n)!=1
      || sscanf(argv[2], "%lf", &x0)!=1
      || sscanf(argv[3], "%lf", &xf)!=1
      || sscanf(argv[4], "%lf", &nx)!=1
      || sscanf(argv[5], "%lf", &y0)!=1
      || sscanf(argv[6], "%lf", &yf)!=1
      || sscanf(argv[7], "%lf", &ny)!=1
      || sscanf(argv[8], "%lf", &tolcnv)!=1
      || sscanf(argv[9], "%d", &maxit)!=1)
   {
      fprintf(stderr,"ERROR: S'ha realitzat una entrada de dades defectuosa\n");
      return 0;
   }	 
   
   Dx=(xf-x0)/nx;
   Dy=(yf-y0)/ny;
  
   for (i=0; i<n; i++)
   {     
      if (scanf("%lf",&u[i])!=1
         || scanf("%lf",&v[i])!=1
         || scanf("%lf",&r[i])!=1
         || scanf("%lf",&g[i])!=1
         || scanf("%lf",&b[i])!=1)
      {
         fprintf(stderr,"ERROR: S'ha realitzat una entrada de dades defectuosa\n");
         return 0;
      }
   }
      if(r[i]>1
      || r[i]<0
      || g[i]>1
      || g[i]<0
      || b[i]>1
      || b[i]<0)
      {
         fprintf(stderr,"ERROR: La component RGB no és vàlida\nSUGGERÈNCIA: El valor de RGB s'ha de trobar en [0,1]\n");
	 return 0;
      } 

   fprintf(stderr,"Calculant...\n");

   for (i=0; i<=nx; i++)
   {
      x=x0+Dx*i;
      for (j=0; j<=ny; j++)
      {
         y=y0+Dy*j;
	 k=cnvnwt (x, y, tolcnv, maxit, n, u, v);
	 if(k==-1)
	 {
	    printf("%.16G\t%.16G\t1\t1\t1\n", x, y);  
	 }
	 else
	 {
	    printf("%.16G\t%.16G\t%d\t%d\t%d\n", x, y,(int)(r[k]*255), (int)(g[k]*255), (int)(b[k]*255));   
	 }
      }
   }
     
   fprintf(stderr,"Càlcul realitzat. El document s'ha escrit amb éxit!\n");
   return 0;
}
   