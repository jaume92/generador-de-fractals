#include <math.h>

void avalp(double x, double y, double *px, double *py, int n, double u[], double v[])
{
   int i;
   double im=0, r=1, b, sx, sy;
   for (i=0; i<n; i++)
   {
      sx=x-u[i];
      sy=y-v[i];
      b=r*sx-im*sy;
      im=r*sy+im*sx;
      r=b;
   }
   *px=r;
   *py=im;
}


void avaldp (double x, double y, double *dpx, double *dpy, int n, double u[], double v[])
{
   int i, j;
   double im, r, sx, sy, b=0, p=0, q=0;
   for (j=0; j<n; j++)
   {
      im=0; r=1;
     
      for (i=0; i<n; i++)
      { 
         if (j==i) continue;
	 sx=x-u[i];
	 sy=y-v[i];
	 b=r*sx-im*sy;
	 im=r*sy+im*sx;
	 r=b;   
      }
      p=p+b;
      q=q+im;
   }
   *dpx=p;
   *dpy=q;
}


int cnvnwt (double x, double y, double tolcnv, int maxit, int n, double u[], double v[])
{
   int i,j;
   double r,rx, ry, dp, px, py, dpx, dpy;

   for (i=0; i<maxit; i++)
   {
      avalp(x, y, &px, &py, n, u, v);
      avaldp(x, y, &dpx, &dpy, n, u, v);
      dp=dpx*dpx+dpy*dpy;
      x=x-(px*dpx+py*dpy)/dp;
      y=y-(py*dpx-px*dpy)/dp;
    
      
      for (j=0; j<n; j++)
      {
         rx=x-u[j];
         ry=y-v[j];
         r=sqrt(rx*rx+ry*ry);
         if (r<tolcnv)  return j;
      }
   }
   return -1;
}
